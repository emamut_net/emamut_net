<?php
// Register Custom Post Type
function portfolio()
{
  $labels = array(
    'name'                => _x( 'Portfolio', 'Post Type General Name', 'portfolio' ),
    'singular_name'       => _x( 'Item', 'Post Type Singular Name', 'portfolio' ),
    'menu_name'           => __( 'Portfolio', 'portfolio' ),
    'name_admin_bar'      => __( 'Portfolio', 'portfolio' ),
    'parent_item_colon'   => __( 'Parent Item:', 'portfolio' ),
    'all_items'           => __( 'Todos Los Items', 'portfolio' ),
    'add_new_item'        => __( 'Añadir Nuevo Item', 'portfolio' ),
    'add_new'             => __( 'Añadir Nuevo', 'portfolio' ),
    'new_item'            => __( 'Nuevo Item', 'portfolio' ),
    'edit_item'           => __( 'Editar Item', 'portfolio' ),
    'update_item'         => __( 'Actualizar Item', 'portfolio' ),
    'view_item'           => __( 'Ver Item', 'portfolio' ),
    'search_items'        => __( 'Buscar Item', 'portfolio' ),
    'not_found'           => __( 'No encontrado', 'portfolio' ),
    'not_found_in_trash'  => __( 'No encontrado en papelera', 'portfolio' ),
  );

  $args = array(
    'label'                 => __( 'portfolio', 'portfolio' ),
    'description'           => __( 'portfolio Custom Post Type', 'portfolio' ),
    'labels'                => $labels,
    'supports'              => array( 'author', 'revisions', 'title' ),
    'hierarchical'          => false,
    'public'                => false,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-admin-links',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => false,
    'capability_type'       => 'post',
    'show_in_rest'          => true,
    'rest_base'             => 'portfolio-api',
    'rest_controller_class' => 'WP_REST_Posts_Controller'
  );
  register_post_type( 'portfolio', $args );
}
add_action( 'init', 'portfolio', 0 );